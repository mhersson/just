mod cli;
mod reporter;
mod structs;
use regex::Regex;
use reporter::{print_issues, print_myworklog, print_timesheet};
use serde_json::json;
use std::{
    fs,
    io::Write,
    process::{Command, Stdio},
};
use structs::{Config, Issues, TimeTrackingUserIssue, Timesheets, Worklogs};

fn main() {
    let matches = cli::build_cli().get_matches();

    match matches.subcommand() {
        ("get", Some(get_matches)) => match get_matches.subcommand() {
            ("all", Some(_)) => print_issues(get_issues(None)),
            ("myworklog", Some(myworklog_matces)) => {
                let date = myworklog_matces.value_of("date");
                get_myworklog(date);
            }
            _ => unreachable!(),
        },
        ("add", Some(add_matches)) => match add_matches.subcommand() {
            ("work", Some(work_matces)) => {
                let key = work_matces.value_of("key").unwrap();
                let time = work_matces.value_of("time").unwrap();
                let date = work_matces.value_of("date");
                let comment = work_matces.value_of("comment");
                add_work(key.to_string(), time.to_string(), date, comment);
            }
            _ => unreachable!(),
        },
        ("open", Some(open_matches)) => open_in_browser(open_matches.value_of("key").unwrap()),
        _ => unreachable!(),
    }
}

fn add_work(key: String, time: String, date: Option<&str>, comment: Option<&str>) {
    validate_issue_key(key.as_str());

    let comment = match comment {
        Some(s) => s.to_string(),
        None => "".to_string(),
    };

    let cfg = load_config(true);

    let url = format!(
        "{}/rest/api/2/issue/{}/worklog",
        cfg.jiraurl,
        key.to_uppercase()
    );

    let payload = json!({
        "comment": comment,
        "started": set_work_start_time(date),
        "timeSpentSeconds": convert_hours_and_minutes_string_to_seconds(time.as_str()),
    });

    let res = http_post_request(cfg, url, payload);

    if res.status() == 201 && !res.text().unwrap().contains("errorMessages") {
        println!("Successfully logged {} on {}", time, key);
    } else {
        println!("Update failed!");
        std::process::exit(1);
    }
}

/// Converts string on format 2h5m to 125 seconds
fn convert_hours_and_minutes_string_to_seconds(time: &str) -> f64 {
    let re = regex::Regex::new("^([0-9.]{1,})([m|h])$").unwrap();
    if re.is_match(time) {
        let cap = re.captures(time).unwrap();

        let num: f64 = cap[1].to_string().parse().unwrap();
        if cap[2].to_string() == "h" {
            return num * 3600.0;
        } else {
            return num * 60.0;
        }
    }

    println!("Invalid duration format");
    std::process::exit(1);
}

fn set_work_start_time(date: Option<&str>) -> String {
    let now = chrono::Utc::now()
        .format("%Y-%m-%dT%H:%M:%S.000+0000")
        .to_string();
    let date = match date {
        Some(s) => s.to_string(),
        None => "".to_string(),
    };

    if date == "" {
        return now;
    }

    validate_date(&date);

    let re =
        regex::Regex::new("202[0-9]-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))").unwrap();

    re.replace(&now, &date).to_string()
}

fn load_config(with_credentials: bool) -> Config {
    let mut filename = match dirs::config_dir() {
        Some(s) => format!("{}/just/config.yaml", s.as_path().display().to_string()),
        None => "config.yaml".to_string(),
    };

    if !std::path::Path::new(&filename).exists() {
        filename = "config.yaml".to_string();
    }

    let contents = match fs::read_to_string(filename) {
        Ok(s) => s,
        Err(e) => {
            println!("Failed to read config file: {}", e);
            std::process::exit(1);
        }
    };

    let mut cfg: Config = match serde_yaml::from_str(&contents) {
        Ok(c) => c,
        Err(e) => {
            println!("Failed to parse config file: {}", e);
            std::process::exit(1);
        }
    };

    cfg.jiraurl = cfg.jiraurl.trim_end_matches("/").to_string();
    if !with_credentials {
        return cfg;
    }

    get_password(&mut cfg);

    cfg
}

fn get_password(cfg: &mut Config) {
    let output = match cfg.passwordtype.as_str() {
        "pass" => {
            Command::new("pass")
                .arg(&cfg.password)
                .output()
                .expect("Failed to get password")
                .stdout
        }
        "gpg" => {
            let pw = base64::decode(cfg.password.as_str()).unwrap();
            let mut cmd = Command::new("gpg")
                .arg("--decrypt")
                .stdin(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()
                .expect("Failed to get password");
            let stdin = cmd.stdin.as_mut().expect("Failed to open stdin");
            stdin.write_all(&pw).expect("Failed to write to stdin");
            cmd.wait_with_output()
                .expect("Failed to read stdout")
                .stdout
        }
        _ => return,
    };

    cfg.password = String::from_utf8_lossy(&output)
        .to_string()
        .trim()
        .to_string();
}

fn validate_issue_key(key: &str) {
    let re = Regex::new("[A-Za-z]{2,9}-[0-9]{1,4}").unwrap();

    if re.is_match(key) {
        return;
    }
    println!("Invalid key");
    std::process::exit(1);
}

fn validate_date(date: &str) -> bool {
    let re = Regex::new("^202[0-9]-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))$").unwrap();

    re.is_match(date)
}

fn open_in_browser(key: &str) {
    validate_issue_key(key);
    let cfg = load_config(false);
    let url = cfg.jiraurl + "/browse/" + &key.to_string();

    match std::env::consts::OS {
        "linux" => {
            Command::new("xdg-open")
                .arg(url)
                .spawn()
                .expect("Failed to open default browser");
        }
        "windows" => {
            Command::new("rundll32")
                .args(["url.dll,FileProtocolHandler", url.as_str()])
                .spawn()
                .expect("Failed to open default browser");
        }
        "darwin" => {
            Command::new("open")
                .arg(url)
                .spawn()
                .expect("Failed to open default browser");
        }
        _ => {
            println!("Unsupported OS");
        }
    }
}

fn get_issues(filter: Option<&str>) -> Issues {
    let cfg: Config = load_config(true);
    let url = format!("{}/rest/api/2/search", cfg.jiraurl);
    let filter = match filter {
        Some(filter) => filter.to_string() + " order by priority, updated",
        None => String::from(
            "assignee = currentUser() AND resolution = Unresolved order by priority, updated",
        ),
    };

    let payload = json!(
    {"jql": filter,
     "startAt":0,
     "maxResults":50,
     "fields":[
         "summary",
         "status",
         "updated",
         "assignee",
         "issuetype",
         "priority"]
    });

    let response = http_post_request(cfg, url, payload);
    // println!("status code = {:?}", &response.status());
    // println!("{:#?}", &response);

    response.json().expect("failed to parse response object")
}

fn get_myworklog(date: Option<&str>) {
    let cfg: Config = load_config(false);

    let date = match date {
        Some(date) => date.to_string(),
        None => chrono::Local::now().format("%Y-%m-%d").to_string(),
    };

    if !validate_date(date.as_str()) {
        println!("Invalid date");
        std::process::exit(1);
    }

    if cfg.useTimesheetPlugin {
        let timesheets = get_timesheet(&date);

        print_timesheet(date, timesheets.worklog);
    } else {
        let filter: String = format!("worklogDate = {} AND worklogAuthor = currentUser()", date);

        let issues = get_issues(Some(&filter));

        let issues = get_time_logged_at_date(cfg.username.as_str(), date.as_str(), issues);

        print_myworklog(issues);
    }
}

fn get_time_logged_at_date(user: &str, date: &str, issues: Issues) -> Vec<TimeTrackingUserIssue> {
    let mut user_issues: Vec<TimeTrackingUserIssue> = vec![];
    for i in issues.into_iter() {
        let time = get_time_logged_on_issue(user, date, &i.key);
        let t: TimeTrackingUserIssue = TimeTrackingUserIssue {
            key: i.key.to_string(),
            date: date.to_string(),
            summary: i.fields.summary.to_string(),
            timeSpent: convert_seconds_to_hours_and_minutes(time),
            timeSpentSeconds: time,
        };

        user_issues.push(t);
    }

    user_issues
}

pub fn convert_seconds_to_hours_and_minutes(time: i32) -> String {
    let hours: i32 = time / 3600;
    let minutes: i32 = (time % 3600) / 60;

    format!("{}h {}m", hours, minutes)
}

fn get_time_logged_on_issue(user: &str, date: &str, key: &str) -> i32 {
    let mut time: i32 = 0;

    let worklogs = get_issue_worklog(key);

    worklogs.into_iter().for_each(|wl| {
        if wl.author.name == user && wl.started.starts_with(&date) {
            time += wl.timeSpentSeconds
        }
    });

    time
}

fn get_issue_worklog(key: &str) -> Worklogs {
    let cfg: Config = load_config(true);
    let url = format!(
        "{}/rest/api/2/issue/{}/worklog",
        cfg.jiraurl,
        &key.to_uppercase()
    );

    let response = http_get_request(cfg, url);

    response.json().expect("failed to parse response object")
}

fn get_timesheet(date: &str) -> Timesheets {
    let cfg: Config = load_config(true);
    let url = format!(
        "{}/rest/timesheet-gadget/1.0/raw-timesheet.json?startDate={}&endDate={}",
        cfg.jiraurl, date, date
    );

    let response = http_get_request(cfg, url);

    response.json().expect("failed to parse response object")
}

fn http_post_request(
    cfg: Config,
    url: String,
    payload: serde_json::Value,
) -> reqwest::blocking::Response {
    let client = reqwest::blocking::Client::new();
    client
        .post(url)
        .json(&payload)
        .basic_auth(cfg.username, Some(cfg.password))
        .send()
        .expect("Post request failed")
}

fn http_get_request(cfg: Config, url: String) -> reqwest::blocking::Response {
    let client = reqwest::blocking::Client::new();
    client
        .get(url)
        .basic_auth(cfg.username, Some(cfg.password))
        .send()
        .expect("Post request failed")
}
