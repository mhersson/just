use crate::convert_seconds_to_hours_and_minutes;
use crate::structs::{Issues, TimeTrackingUserIssue, Timesheet};
use ansi_term::Color::{Blue, Cyan, Green, Purple, Red, Yellow};
use ansi_term::Style;

fn format_issue_type(issue_type: &str) -> String {
    match issue_type {
        "Improvement" => format!("{}{:<12}{}", Green.prefix(), issue_type, Green.suffix()),
        "Task" => format!("{}{:<12}{}", Blue.prefix(), issue_type, Blue.suffix()),
        "Red" => format!("{}{:<12}{}", Red.prefix(), issue_type, Red.suffix()),
        "Epic" | "Story" => format!("{}{:<12}{}", Purple.prefix(), issue_type, Purple.suffix()),
        "Setup" => format!("{}{:<12}{}", Cyan.prefix(), issue_type, Cyan.suffix()),
        _ => format!("{:<12}", issue_type),
    }
}

fn format_priority(priority: &str) -> String {
    match priority {
        "Low" => format!("{}{:<10}{}", Green.prefix(), priority, Green.suffix()),
        "Normal" => format!("{}{:<10}{}", Blue.prefix(), priority, Blue.suffix()),
        "Critical" | "High" | "Blocker" => {
            format!("{}{:<10}{}", Red.prefix(), priority, Red.suffix())
        }
        _ => format!("{}", priority),
    }
}

fn format_status(status: &str) -> String {
    match status {
        "Closed" | "Resolved" | "Verified" => {
            format!("{}{:<20}{}", Green.prefix(), status, Green.suffix())
        }
        "To Be Fixed" | "In Progress" | "Accepted" | "Awaiting info" => {
            format!("{}{:<20}{}", Blue.prefix(), status, Blue.suffix())
        }
        "Programmed" | "Peer Review" | "Ready for Test" | "Ready for review" => {
            format!("{}{:<20}{}", Cyan.prefix(), status, Cyan.suffix())
        }
        "Rejected" => {
            format!("{}{:<20}{}", Red.prefix(), status, Red.suffix())
        }

        "New" | "Open" => format!(
            "{}{:<20}{}",
            Style::new().bold().prefix(),
            status,
            Style::new().bold().suffix()
        ),
        _ => format!("{}", status),
    }
}

pub fn print_issues(issues: Issues) {
    println!(
        "{}{:<15}{:<12}{:<10}{:<64}{:<20}{:<15}{}",
        Yellow.underline().prefix(),
        "Key",
        "Type",
        "Priority",
        "Summary",
        "Status",
        "Assignee",
        Yellow.suffix(),
    );
    for i in issues.issues.iter() {
        println!(
            "{:<15}{}{}{:<64}{}{:<15}",
            i.key,
            format_issue_type(i.fields.issuetype.name.as_str()),
            format_priority(i.fields.priority.name.as_str()),
            truncate_string(i.fields.summary.to_string(), 60),
            format_status(i.fields.status.name.as_str()),
            i.fields.assignee.displayName
        );
    }
}

pub fn print_myworklog(issues: Vec<TimeTrackingUserIssue>) {
    if issues.len() >= 1 {
        println!(
            "{}{:<12}{:<15}{:<61}{}{}",
            Yellow.underline().prefix(),
            "Date",
            "Key",
            "Summary",
            "Time Spent",
            Yellow.suffix(),
        );

        let mut total = 0;

        for i in issues.into_iter() {
            let summary = truncate_string(i.summary, 60);

            println!("{:<12}{:<15}{:<64}{}", i.date, i.key, summary, i.timeSpent);
            total += i.timeSpentSeconds;
        }

        println!(
            "{}{}Total time spent: {}{}",
            " ".repeat(73),
            Style::new().underline().prefix(),
            convert_seconds_to_hours_and_minutes(total),
            Style::new().underline().suffix()
        );
    } else {
        println!("You have not logged any hours on this date");
    }
}

pub fn print_timesheet(date: String, timesheets: Vec<Timesheet>) {
    if timesheets.len() >= 1 {
        println!(
            "{}{:<12}{:<15}{:<61}{}{}",
            Yellow.underline().prefix(),
            "Date",
            "Key",
            "Summary",
            "Time Spent",
            Yellow.suffix(),
        );

        let mut total = 0;

        for ts in timesheets.into_iter() {
            let mut secs: i32 = 0;
            let summary = truncate_string(ts.summary, 60);

            for e in ts.entries {
                secs += e.timeSpent;
            }
            println!(
                "{:<12}{:<15}{:<64}{}",
                date,
                ts.key,
                summary,
                convert_seconds_to_hours_and_minutes(secs)
            );
            total += secs
        }

        println!(
            "{}{}Total time spent: {}{}",
            " ".repeat(73),
            Style::new().underline().prefix(),
            convert_seconds_to_hours_and_minutes(total),
            Style::new().underline().suffix()
        );
    } else {
        println!("You have not logged any hours on this date");
    }
}

fn truncate_string(str: String, size: usize) -> String {
    let mut str = str;
    if str.len() > size {
        str.truncate(size);
        str = str + "..";
    }
    str
}
