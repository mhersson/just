use serde::Deserialize;
use serde::Serialize;

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub jiraurl: String,
    pub username: String,
    pub password: String,
    pub passwordtype: String,
    pub useTimesheetPlugin: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Issues {
    pub issues: Vec<Issue>,
}

impl<'a> std::iter::IntoIterator for &'a Issues {
    type Item = <std::slice::Iter<'a, Issue> as Iterator>::Item;
    type IntoIter = std::slice::Iter<'a, Issue>;

    fn into_iter(self) -> Self::IntoIter {
        self.issues.as_slice().into_iter()
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct Issue {
    pub id: String,
    pub key: String,
    pub fields: IssueFields,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IssueFields {
    pub summary: String,
    pub issuetype: IssueType,
    pub assignee: Assignee,
    pub priority: Priority,
    pub status: Status,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IssueType {
    pub id: String,
    pub name: String,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Assignee {
    pub name: String,
    pub emailAddress: String,
    pub displayName: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Status {
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Priority {
    pub id: String,
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Worklogs {
    pub worklogs: Vec<Worklog>,
}

impl<'a> std::iter::IntoIterator for &'a Worklogs {
    type Item = <std::slice::Iter<'a, Worklog> as Iterator>::Item;
    type IntoIter = std::slice::Iter<'a, Worklog>;

    fn into_iter(self) -> Self::IntoIter {
        self.worklogs.as_slice().into_iter()
    }
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Worklog {
    pub author: Author,
    pub comment: Option<String>,
    pub created: String,
    pub started: String,
    pub timeSpent: String,
    pub timeSpentSeconds: i32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Timesheets {
    pub worklog: Vec<Timesheet>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Timesheet {
    pub key: String,
    pub summary: String,
    pub entries: Vec<TimesheetEntry>,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct TimesheetEntry {
    pub timeSpent: i32,
}

#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct Author {
    pub name: String,
    pub displayName: String,
}

// Struct for representing the time a user
// has spent on an issue on a given date.
#[allow(non_snake_case)]
#[derive(Debug, Serialize, Deserialize)]
pub struct TimeTrackingUserIssue {
    pub key: String,
    pub date: String,
    pub summary: String,
    pub timeSpent: String,
    pub timeSpentSeconds: i32,
}
