use clap::{crate_version, App, AppSettings, Arg};

pub fn build_cli() -> App<'static, 'static> {
    App::new("just")
        .version(crate_version!())
        .about("A command line JIRA app written in Rust")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .subcommand(
            App::new("get")
                .about("Get issues and worklogs")
                .subcommand(
                    App::new("all").about("Get all unresolved issues assigned to the current user"),
                )
                .subcommand(
                    App::new("myworklog")
                        .about("Get the worklog for the current user at a give date")
                        .arg(
                            Arg::with_name("date")
                                .help("The date of the worklog")
                                .takes_value(true),
                        ),
                ),
        )
        .subcommand(
            App::new("add").about("Add a new worklog").subcommand(
                App::new("work")
                    .about("Register work on issue")
                    .arg(Arg::with_name("key").help("The issue key").required(true))
                    .arg(
                        Arg::with_name("time")
                            .help("The time (on format 1.5h or 90m)")
                            .required(true),
                    )
                    .arg(
                        Arg::with_name("date")
                            .help("The date the work was done (default=today)")
                            .short("d")
                            .long("date"),
                    )
                    .arg(
                        Arg::with_name("comment")
                            .help("Add a comment to the work log")
                            .short("c")
                            .long("comment")
                            .takes_value(true),
                    ),
            ),
        )
        .subcommand(
            App::new("open")
                .about("Open issue in browser")
                .arg(Arg::with_name("key").help("The issue key").required(true)),
        )
}
