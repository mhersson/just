## Just, a JIRA app written in Rust


### Key Features:
  - Report your time on issues
  - List all issues assigned to you
  - Open issue in default browser
  

Just integrates with passwordstore and gpg to keep your password safe.

### Install Instructions
After building make sure the just executable is in your path.<br/>
Copy the `config-example.yaml` to $HOME/.config/just/config.yaml<br/>
and edit the JIRA server setting, username and password.<br/>
