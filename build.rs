use std::path::PathBuf;

use clap::Shell;

include!("src/cli.rs");

fn main() {
    let path: PathBuf = [env!("CARGO_MANIFEST_DIR"), "completions"].iter().collect();
    let mut app: App = build_cli();

    app.gen_completions("just", Shell::Bash, &path);
    app.gen_completions("just", Shell::Zsh, &path);
}
